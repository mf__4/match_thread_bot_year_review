import string
import mysql.connector
import sys
import praw
from time import sleep
from typing import List
import requests

proper_teams = ["Rangers", "Celtic", "Hibernian", "Aberdeen", "Heart of Midlothian", "Dundee United", "Ross County",
                "Livingston","St Johnstone", "Motherwell", "St Mirren", "Kilmarnock", "Scotland", "Ayr United",
                "Greenock Morton", "Dundee", "Queen's Park", "Partick Thistle", "Inverness CT", "Raith Rovers",
                "Elgin City", "Bonnyrigg Rose", "Cove Rangers", "Arbroath", "Hamilton Academical", "Albion Rovers",
                "Dunfermline Athletic", "Falkirk", "Clyde", "Cowdenbeath", "East Stirlingshire", "Brechin City",
                "Newton Stewart", "Queen of the South", "Morton Reserves", "Dumbarton", "Kelty Hearts",
                "Stirling Albion", "Stranraer", "Stenhousemuir", "East Fife", "Linlithgow Rose", "East Kilbride",
                "Forfar Athletic", "Alloa Athletic"]
def setup():
    try:
        admin, username, password, subreddit, user_agent, id, secret, redirect = tuple(sys.argv[1:9])
        r = praw.Reddit(client_id=id, client_secret=secret, username=username, password=password, user_agent=user_agent)
        return r, admin, username, password, subreddit, user_agent, id, secret, redirect
    except:
        sleep(10)


def get_cursor():
    mydb = mysql.connector.connect(
        host="mtb-year-review-db",
        user="bot",
        password="bot"
    )
    cursor = mydb.cursor()

    return cursor, mydb


def query_db(query) -> List:
    if "time_posted BETWEEN \'2022-07-09 00:00:00\' AND \'2023-06-05 23:59:59\'" not in query:
        print("query is not time limited!!!")
    print(f"Executing query {query}")
    cursor, mydb = get_cursor()

    cursor.execute(query)
    result = cursor.fetchall()
    return result


def top_threads() -> str:
    body = "#Posts\n"

    def construct_table(results, type="Score") -> str:

        table_body = f"|Match Thread|{type}|Top Comment|\n|:--|:--:|:--:|:--:|\n"

        for res in results:
            post_id, title, score, time_posted = res
            title = title.replace("|", "/")
            date_posted = f"{time_posted.day:02d}/{time_posted.month:02d}/{time_posted.year}"

            top_comment_query = f"SELECT body,comment_id,score,commentor FROM tm.comments " \
                                f"WHERE post='{post_id}' " \
                                f"AND (time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                                f"ORDER BY score DESC LIMIT 1"
            top_comment_res = query_db(top_comment_query)
            comment_body, comment_id, comment_score, comment_user = top_comment_res[0]
            comment_body = comment_body.replace("\n", "\t")

            table_body += f"|[{title}](https://reddit.com/{post_id}) ({date_posted})|{score:,}|[{comment_body}](https://reddit.com/r/scottishfootball/comments/{post_id}/comment/{comment_id}) by u/{comment_user} ({comment_score})|\n"
        return table_body

    ## score
    body += "##Top threads by score\n"
    # incl all teams
    body += "### For all teams\n"
    query = f"SELECT post_id,title,score,time_posted FROM tm.post " \
            f"WHERE (time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
            f"ORDER BY SCORE DESC LIMIT 5"
    result = query_db(query)
    body += construct_table(result)


    # without old firm and nt
    body += "### Not including the Old Firm or National Team\n"
    query = f"SELECT post_id,title,score,time_posted FROM tm.post " \
            f"WHERE (time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
            f"AND NOT (team_1 = 'Rangers' OR team_1 = 'Celtic' OR team_1 = 'Scotland' OR team_2 = 'Rangers' OR team_2 = 'Celtic' OR team_2 = 'Scotland') " \
            f"ORDER BY SCORE DESC LIMIT 5"
    result = query_db(query)
    body += construct_table(result)

    body += "## Top threads by comments\n"
    ## comments
    # incl all teams
    body += "### For all teams\n"
    query = f"SELECT post_id,title,COUNT(comment_id),tm.post.time_posted FROM tm.post " \
            f"JOIN tm.comments ON tm.comments.post = tm.post.post_id " \
            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
            f"GROUP BY post_id " \
            f"ORDER BY COUNT(comment_id) DESC LIMIT 5"
    result = query_db(query)
    body += construct_table(result, type="Comments")


    # without old firm and nt
    body += "### Not including the Old Firm or National Team\n"
    query = f"SELECT post_id,title,COUNT(comment_id),tm.post.time_posted FROM tm.post " \
            f"JOIN tm.comments ON tm.comments.post = tm.post.post_id " \
            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
            f"AND NOT (team_1 = 'Rangers' OR team_1 = 'Celtic' OR team_1 = 'Scotland' OR team_2 = 'Rangers' OR team_2 = 'Celtic' OR team_2 = 'Scotland') " \
            f"GROUP BY post_id " \
            f"ORDER BY COUNT(comment_id) DESC LIMIT 5"
    result = query_db(query)
    body += construct_table(result, type="Comments")


    return body


def opening_paragraph() -> str:
    number_posts_query = f"SELECT COUNT(post_id) FROM tm.post " \
                         f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "

    number_comments_query = f"SELECT COUNT(comment_id) FROM tm.comments " \
                            f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "

    number_users_query = f"SELECT COUNT(distinct commentor) FROM tm.comments " \
                         f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                         f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "

    avg_comments_query = f"SELECT AVG(comments.score),AVG(post.score) FROM tm.comments " \
                         f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                         f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "

    number_posts = query_db(number_posts_query)[0][0]
    number_comments = query_db(number_comments_query)[0][0]
    number_users = query_db(number_users_query)[0][0]
    avg_comment_score, avg_post_score = query_db(avg_comments_query)[0]

    body = f"Hi all, posted a similar thread at the end of 2022 which had a shitload of stats about the Match Threads " \
           f"for all who care about that sort of thing. That thread can be found [here](https://www.reddit.com/zyyv7m). " \
           f"Over the 2022-23 season there have been {number_posts:,} Match Threads, {number_comments:,} comments on those threads " \
           f"and {number_users:,} unique people commenting on them. \n\nThe average score on each post was {round(avg_post_score, 2):,}, " \
           f"the average score on a comment was {round(avg_comment_score, 2):,} and there were {round(number_comments/number_posts, 2):,} comments on each thread " \
           f"on average.\n\nHeres a bunch of stats about all those threads: \n\n"

    return f"{body}\n"

def comments_opening() -> str:

    number_comments_query = f"SELECT COUNT(comment_id) FROM tm.comments " \
                            f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "

    number_users_query = f"SELECT COUNT(distinct commentor) FROM tm.comments " \
                         f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                         f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "

    comment_karma_query = f"SELECT SUM(comments.score) FROM tm.comments " \
                         f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                         f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "

    halftimepets_num_query = f"SELECT COUNT(*) FROM tm.comments " \
                             f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                             f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59' " \
                             f"AND tm.comments.body LIKE '%half time pet%') "

    halftimepets_karma_query = f"SELECT SUM(comments.score) FROM tm.comments " \
                               f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                               f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59' " \
                               f"AND tm.comments.body LIKE '%half time pet%') "


    number_comments = query_db(number_comments_query)[0][0]
    number_users = query_db(number_users_query)[0][0]
    comment_karma_total = query_db(comment_karma_query)[0][0]
    htp_comments_total = query_db(halftimepets_num_query)[0][0]
    htp_karma_total = query_db(halftimepets_karma_query)[0][0]

    body = f"The average number of comments made by each user was {round(number_comments/number_users,2):,}, " \
           f"{comment_karma_total:,} karma was given out in total in the comments, an average of " \
           f"{round(comment_karma_total/number_users, 2):,} per user. There was also {htp_comments_total:,} comments with" \
           f" the phrase 'half time pet' in them, gaining a total of {htp_karma_total:,} karma."

    return f"{body}\n"


def average_comments() -> str:
    def construct_table(table_info, table_type="team") -> str:
        if table_type == "team":
            table_body = f"|Team|Number of comments|Number of threads|Average comments per thread|Most popular thread|Top Commenter|\n|:--|:--|:--|:--|:--|:--|:--|\n"
        else:
            table_body = f"|Competition|Number of comments|Number of threads|Average comments per thread|Top Commenter|\n|:--|:--|:--|:--|:--|:--|\n"

        for res in sorted(table_info.items(), key=lambda x: x[1], reverse=True)[0:15]:
            team, count = res

            team = team.replace("'", "\\'")

            top_commentor_query = f"SELECT commentor,COUNT(comment_id),SUM(tm.comments.score) FROM tm.comments " \
                                  f"JOIN tm.users ON tm.users.reddit_username=tm.comments.commentor " \
                                  f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                                  f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                                  f"AND (tm.post.team_1 = '{team}' OR tm.post.team_2 = '{team}') " \
                                  f"AND commentor <> 'deleted' " \
                                  f"GROUP BY commentor " \
                                  f"ORDER BY COUNT(comment_id) DESC " \
                                  f"LIMIT 1"
            top_commentor_res = query_db(top_commentor_query)
            commentor, total_comments, total_score = top_commentor_res[0]

            num_threads = f"SELECT COUNT(post_id) FROM tm.post " \
                          f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                          f"AND (team_1='{team}' OR team_2='{team}')"
            num_threads_res = query_db(num_threads)
            num_threads = num_threads_res[0][0]
            if num_threads == 0:
                num_threads = -1

            if team in ["Rangers", "Celtic"]:
                other_team = "Rangers" if team == "Celtic" else "Celtic"
                thread_line = f"AND (team_1<>'{other_team}' AND team_2<>'{other_team}')"
            else:
                thread_line = f"AND (team_1<>'Rangers' AND team_2<>'Rangers' AND team_1<>'Celtic' AND team_2<>'Celtic')"

            best_thread_q = "SELECT title,tm.post.score,post_id,tm.post.time_posted,COUNT(comment_id) FROM tm.post "\
                            "JOIN tm.comments ON tm.comments.post=tm.post.post_id "\
                            "WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "\
                            f"AND (team_1 = '{team}' OR team_2 = '{team}') " \
                            f"{thread_line} " \
                            f"GROUP BY post_id "\
                            "ORDER BY COUNT(comment_id) DESC "\
                            "LIMIT 1"

            title, best_thread_score, post_id, time_posted, comment_count = query_db(best_thread_q)[0]
            title = title.replace("|", "/")
            date_posted = f"{time_posted.day:02d}/{time_posted.month:02d}/{time_posted.year}"

            table_body += f"|{team}|{count:,}|{num_threads}" \
                          f"|{round(count / num_threads, 2)}" \
                          f"|[{title} ({date_posted})](https://reddit.com/{post_id}) ({best_thread_score:,} karma/{comment_count:,} comments)" \
                          f"|u/{commentor} ({total_comments:,} comments, {total_score:,} karma)|\n"
        return table_body

    body = ""
    body += "### Most comments \n"
    body += f"|User|Count|\n|:--|:--|\n"
    top_commentors = "SELECT COUNT(comment_id), commentor FROM tm.comments " \
                     "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                     "JOIN tm.users ON tm.users.reddit_username = tm.comments.commentor " \
                     f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                     f" AND tm.comments.commentor<>'deleted'  " \
                     "GROUP BY commentor " \
                     "ORDER BY COUNT(comment_id) DESC " \
                     "LIMIT 10"

    most_comment_res = query_db(top_commentors)

    for x in most_comment_res:
        comment_count, commentor = x
        body += f"|u/{commentor}|{comment_count:,}|\n"

    body += "##Total comments by team\nThis is the number of comments made in the threads for each team. " \
            "For the 'Most popular thread' column, this cannot involve an Old Firm match or either of the " \
            "Old Firm (otherwise they're all Rangers/Celtic games). \n\n"
    query = "SELECT team_1,COUNT(comment_id) FROM tm.comments " \
            "JOIN tm.users ON tm.users.reddit_username=tm.comments.commentor " \
            f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
            "AND (team_1 <>'Unknown' AND team_2 <>'Unknown') " \
            "GROUP BY team_1 " \
            "ORDER BY COUNT(comment_id) DESC "

    team_1 = query_db(query)


    query = "SELECT team_2,COUNT(comment_id) FROM tm.comments " \
            "JOIN tm.users ON tm.users.reddit_username=tm.comments.commentor " \
            f"JOIN tm.post ON tm.post.post_id = tm.comments.post " \
            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
            "AND (team_1 <>'Unknown' AND team_2 <>'Unknown') " \
            "GROUP BY team_2 " \
            "ORDER BY COUNT(comment_id) DESC " \
            "LIMIT 12 "
    team_2 = query_db(query)
    team_comments = {}
    for res in team_1:
        team, count = res
        if team not in proper_teams:
            continue
        if team not in team_comments.keys():
            team_comments[team] = 0
        team_comments[team] += count

    for res in team_2:
        team, count = res
        if team not in proper_teams:
            continue
        if team not in team_comments.keys():
            team_comments[team] = 0
        team_comments[team] += count


    body += construct_table(team_comments)

    #  todo competition
    return body


def comments_of_the_year() -> str:
    body = ""

    def construct_table(table_contents):
        table_body = f"|Comment|User|Thread|Score|\n|:--|:--|:--|:--|:--|\n"
        for res in table_contents:
            comment, comment_id, user, score, title, thread_id = res
            comment = comment.replace("\n", "\t")
            title = title.replace("|", "/")
            table_body += f"|[{comment}](https://reddit.com/r/scottishfootball/comments/{thread_id}/comment/{comment_id})|u/{user}|[{title}](https://reddit.com/{thread_id})|{score}|\n"

        return table_body

    body += "##Top 5 comments\n"
    query = "SELECT body,comment_id,commentor,tm.comments.score,title,post FROM tm.comments " \
            "JOIN tm.post ON tm.post.post_id=tm.comments.post " \
            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
            "ORDER BY score DESC " \
            "LIMIT 5"
    result = query_db(query)
    body += construct_table(result)

    body += "##Bottom 5 comments\n"
    query = "SELECT body,comment_id,commentor,tm.comments.score,title, post FROM tm.comments " \
            "JOIN tm.post ON tm.post.post_id=tm.comments.post " \
            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
            "ORDER BY score ASC " \
            "LIMIT 5"
    result = query_db(query)
    body += construct_table(result)

    # print(body)
    return body


def half_time_pets() -> str:
    body = ""

    def construct_table(table_contents):
        table_body = f"|Comment|User|Thread|Score|\n|:--|:--|:--|:--|:--|\n"
        for res in table_contents:
            comment, comment_id, user, score, title, thread_id = res
            comment = comment.replace("\n", "\t")
            title = title.replace("|", "/")
            table_body += f"|[{comment}](https://reddit.com/r/scottishfootball/comments/{thread_id}/comment/{comment_id})|u/{user}|[{title}](https://reddit.com/{thread_id})|{score}|\n"

        return table_body

    body += "##Top 20 half time pets!\nThe formatting is a bit weird here, apologies but I cba fixing it, I'll make up for it by having the top 20 instead of top 5 :)\n\n"
    query = "SELECT body,comment_id,commentor,tm.comments.score,title,post FROM tm.comments " \
            "JOIN tm.post ON tm.post.post_id=tm.comments.post " \
            f"WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59' AND tm.comments.body LIKE '%half time pet%') " \
            "ORDER BY score DESC " \
            "LIMIT 20"
    result = query_db(query)
    body += construct_table(result)

    return body


def individual_awards() -> str:
    body = ""
    body += "## Top Commentors\n"  # largest amount of karma (total and average)?

    most_comments_query = "SELECT commentor,COUNT(comment_id) FROM tm.comments " \
                          "JOIN tm.users ON tm.users.reddit_username = tm.comments.commentor " \
                          "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                          "WHERE commentor<>'deleted' " \
                          "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                          "GROUP BY commentor " \
                          "ORDER BY COUNT(comment_id) DESC " \
                          "LIMIT 1 "

    most_comments_res = query_db(most_comments_query)
    top_karma_query = "SELECT commentor,SUM(comments.score),AVG(comments.score) FROM tm.comments " \
                      "JOIN tm.users ON tm.users.reddit_username = tm.comments.commentor " \
                      "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                      "WHERE commentor<>'deleted' " \
                      "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                      "GROUP BY commentor " \
                      "ORDER BY SUM(comments.score) DESC " \
                      "LIMIT 1 "
    top_karma_res = query_db(top_karma_query)

    avg_karma_query = "SELECT commentor,AVG(comments.score),COUNT(comment_id) FROM tm.comments " \
                      "JOIN tm.users ON tm.users.reddit_username = tm.comments.commentor " \
                      "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                      "WHERE commentor<>'deleted' " \
                      "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                      "GROUP BY commentor " \
                      "HAVING COUNT(comment_id) > 20 " \
                      "ORDER BY AVG(comments.score) DESC " \
                      "LIMIT 100 "

    avg_karma_res = query_db(avg_karma_query)

    top_comments_user, top_comments_total = most_comments_res[0]
    top_karma_user, top_karma_total, avg_top_karma = top_karma_res[0]
    avg_karma_user, avg_karma_total, avg_total_comments = avg_karma_res[0]

    body += f"Congratulations to u/{top_comments_user} who has made the most amount of comments in total ({top_comments_total:,}) this year!\n\n"
    body += f"Congratulations to u/{top_karma_user} who has made the most karma ({top_karma_total:,} avg: {round(avg_top_karma, 2)}) this year!\n\n"
    body += f"Congratulations to u/{avg_karma_user} who was the most popular commentor, with an average karma of {round(avg_karma_total, 2)} over {avg_total_comments:,} comments this year!\n\n"

    body += "## Bottom Commentors\n"  # smallest amount of average karma by month?

    bot_karma_query = "SELECT commentor,SUM(comments.score),AVG(comments.score) FROM tm.comments " \
                      "JOIN tm.users ON tm.users.reddit_username = tm.comments.commentor " \
                      "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                      "WHERE commentor<>'deleted' " \
                      "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                      "GROUP BY commentor " \
                      "HAVING SUM(comments.score) < -10 " \
                      "ORDER BY SUM(comments.score) ASC " \
                      "LIMIT 1 "
    bot_karma_res = query_db(bot_karma_query)

    avg_karma_query = "SELECT commentor,AVG(comments.score),COUNT(comment_id) FROM tm.comments " \
                      "JOIN tm.users ON tm.users.reddit_username = tm.comments.commentor " \
                      "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                      "WHERE commentor<>'deleted' " \
                      "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                      "GROUP BY commentor " \
                      "HAVING COUNT(comment_id) > 20 " \
                      "ORDER BY AVG(comments.score) ASC " \
                      "LIMIT 100 "

    avg_karma_res = query_db(avg_karma_query)

    bot_karma_user, bot_karma_total, avg_bot_karma = bot_karma_res[0]
    avg_karma_user, avg_karma_total, avg_total_comments = avg_karma_res[0]

    body += f"Commiserations to u/{bot_karma_user} who has made the least karma ({bot_karma_total:,} avg: {round(avg_bot_karma, 2)}) this year\n\n"
    body += f"Commiserations to u/{avg_karma_user} who was the least popular commentor, with an average karma of {round(avg_karma_total, 2)} over {avg_total_comments:,} comments this year\n\n"

    body += "## Most Obsessed\n"  # comments in threads of other teams, maybe rival
    most_obsessed_all_query = "SELECT commentor,COUNT(comment_id),team,AVG(comments.score) FROM tm.comments " \
                        "JOIN tm.users ON tm.users.reddit_username = tm.comments.commentor " \
                        "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                        "WHERE (tm.users.team <> tm.post.team_1 AND tm.users.team <> tm.post.team_2) " \
                        "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                        "AND commentor<>'deleted' "  \
                        "AND team<>'Unknown' "  \
                        "GROUP BY commentor " \
                        "ORDER BY COUNT(comment_id) DESC " \
                        "LIMIT 10 "

    most_obsessed_all_res = query_db(most_obsessed_all_query)

    most_obsessed_of_query = "SELECT commentor,COUNT(comment_id),team,AVG(comments.score),SUM(comments.score) FROM tm.comments " \
                        "JOIN tm.users ON tm.users.reddit_username = tm.comments.commentor " \
                        "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                        "WHERE (tm.users.team <> tm.post.team_1 AND tm.users.team <> tm.post.team_2) " \
                        "AND (tm.users.team='Rangers' OR tm.users.team='Celtic') "  \
                        "AND (tm.post.team_1 = 'Rangers' OR tm.post.team_1 = 'Celtic' OR tm.post.team_2 = 'Rangers' OR tm.post.team_2 = 'Celtic') " \
                        "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                        "AND commentor<>'deleted' "  \
                        "AND team<>'Unknown' "  \
                        "GROUP BY commentor " \
                        "ORDER BY COUNT(comment_id) DESC " \
                        "LIMIT 6 "

    most_obsessed_of_res = query_db(most_obsessed_of_query)
    most_obs_commentor_all, most_obs_commentor_all_com, most_obs_commentor_all_team, most_obs_commentor_all_avg = most_obsessed_all_res[0]
    body += f"Congratulations to u/{most_obs_commentor_all} who has made {most_obs_commentor_all_com} comments in " \
            f"other teams threads this year!\n"

    body += "## Most Obssesed (Old Firm Edition)\n"
    most_obs_commentor_of, most_obs_commentor_of_com, most_obs_commentor_of_team, most_obs_commentor_of_avg, most_obs_commentor_of_total = most_obsessed_of_res[0]
    other_of = "Celtic" if most_obs_commentor_of_team == "Rangers" else "Rangers"#

    total_comments = query_db("SELECT COUNT(comment_id) FROM tm.comments "
                              "JOIN tm.post ON tm.post.post_id = tm.comments.post "
                              "WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "
                              f"AND commentor='{most_obs_commentor_of}'")[0][0]

    body += f"Congratulations to u/{most_obs_commentor_of} who supports {most_obs_commentor_of_team} but has made " \
            f"{most_obs_commentor_of_com} comments in {other_of} threads this year " \
            f"({round(most_obs_commentor_of_com/total_comments,2)*100}% of their total comments)! They got an " \
            f"average karma of {round(most_obs_commentor_of_avg,2)} on their comments there.\n"

    body += "###Best of the rest:\n"
    body += f"|User|Team|Number of comments|% of all comments|Karma|Best recieved comment|Worst recieved comment|\n|:--|:--|:--|:--|:--|:--|:--|:--|\n"
    for res in most_obsessed_of_res[1:]:

        commentor, num_comments, team, average_score, total_score = res



        other_of = "Celtic" if team == "Rangers" else "Rangers"

        best_com_q = "SELECT comments.body,comments.comment_id,post.post_id,comments.score FROM tm.comments " \
                     "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                     f"WHERE (tm.post.team_1 <> '{team}' AND tm.post.team_2 <> '{team}') " \
                     f"AND (tm.post.team_1 = '{other_of}' OR tm.post.team_2 = '{other_of}') " \
                     "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                     f"AND commentor='{commentor}'" \
                     "ORDER BY comments.score DESC " \
                     "LIMIT 1"

        worst_com_q = "SELECT comments.body,comments.comment_id,post.post_id,comments.score FROM tm.comments " \
                     "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                     f"WHERE (tm.post.team_1 <> '{team}' AND tm.post.team_2 <> '{team}') " \
                     f"AND (tm.post.team_1 = '{other_of}' OR tm.post.team_2 = '{other_of}') " \
                     "AND (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') " \
                     f"AND commentor='{commentor}'" \
                     "ORDER BY comments.score ASC " \
                     "LIMIT 1"



        best_bod, best_com_id, best_post_id, best_com_score = query_db(best_com_q)[0]
        worst_bod, worst_com_id, worst_post_id, worst_com_score = query_db(worst_com_q)[0]

        total_comments = query_db("SELECT COUNT(comment_id) FROM tm.comments "
                                  "JOIN tm.post ON tm.post.post_id = tm.comments.post "
                                  "WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "
                                  f"AND commentor='{most_obs_commentor_of}'")[0][0]

        best_bod = best_bod.replace("\n", "\t")
        worst_bod = worst_bod.replace("\n", "\t")

        body += f"|u/{commentor}|{team}|{num_comments:,}|{round(num_comments/total_comments, 2)*100}%|{total_score:,}|" \
                f"[{best_bod}](https://reddit.com/r/scottishfootball/comments/{best_post_id}/comment/{best_com_id}) (Karma: {best_com_score})|" \
                f"[{worst_bod}](https://reddit.com/r/scottishfootball/comments/{worst_post_id}/comment/{worst_com_id}) (Karma: {worst_com_score})|\n"




    return body


def words() -> str:
    swears = ["fuck", "fucking", "fucked", "fucks", "fuck's", "fuckin",
              "cunt", "cunts",
              "shitting", "shit", "shited", "shiting", "shite",
              "bastard", "bastards"]

    body = "# Words, words, words\n"
    get_all_comment_bodies = "SELECT body, commentor FROM tm.comments " \
                              "JOIN tm.post ON tm.post.post_id = tm.comments.post " \
                              "WHERE (tm.post.time_posted BETWEEN '2022-07-09 00:00:00' AND '2023-06-05 23:59:59') "
    get_all_comment_res = query_db(get_all_comment_bodies)

    words = {}
    commentor_total_swears = {}
    commentor_total_words = {}
    count = 0
    for res in get_all_comment_res:
        comment_body, commentor = res

        if commentor not in commentor_total_swears.keys():
            commentor_total_swears[commentor] = 0

        if commentor not in commentor_total_words.keys():
            commentor_total_words[commentor] = 0

        comment_body = comment_body.translate(str.maketrans('', '', string.punctuation))
        word_list = comment_body.split(" ")
        for w in word_list:
            count += 1

            if w not in words.keys():
                words[w] = 0

            if commentor != 'deleted':
                commentor_total_words[commentor] = commentor_total_words[commentor] + 1

            if w in swears and commentor != 'deleted':
                commentor_total_swears[commentor] = commentor_total_swears[commentor] + 1

            words[w] = words[w] + 1

    swear_lines = {}
    swear_count = 0
    for swear in swears:
        if swear not in words.keys():
            continue
        swear_count += words[swear]
        swear_lines[words[swear]] = f"|{swear}|{words[swear]}|\n"

    body += f"There was {count:,} words said over all comments, of which {swear_count:,} were one of the below swear " \
           f"words ({round((swear_count/count)*100,2)}%).\n"


    body += "## Swear Jar\n"

    body += f"|Word|Count|\n|:--|:--|\n"
    for x in sorted(swear_lines.items(), key=lambda x: x[0], reverse=True):
        score, line = x
        body += line

    body += "## Biggest Swearers\n"
    body += f"|Word|Count|% of words as swears|\n|:--|:--|:--|\n"
    for x in sorted(commentor_total_swears.items(), key=lambda x: x[1], reverse=True)[:5]:
        user, count = x
        total_words = commentor_total_words[user]
        body += f"|u/{user}|{count}|{round((count/total_words)*100,2):,}%|\n"



    return body


def closing_paragraph() -> str:
    return "\n\nCheers to all for participating the threads throughout the season, hopefully I got all of the stats " \
           "right, if something seems wrong @ my personal account (u/mf__4) and I'll check it. The script I used " \
           "to gather the data from reddit into a mysql db and to then construct this post is available " \
           "[here](https://gitlab.com/mf__4/match_thread_bot_year_review/), happy to answer any questions on it " \
           "(unless youre moaning about dodgy practices). "


def construct_body() -> str:

    body = opening_paragraph()

    body += "#Individual Awards\n"
    body += individual_awards()

    body += words()

    body += top_threads()

    body += "# Comments\n"
    body += comments_opening()

    body += average_comments()
    body += comments_of_the_year()
    body += half_time_pets()


    body += closing_paragraph()

    return body


if __name__ == "__main__":
    r, admin, username, password, subreddit, user_agent, id, secret, redirect = setup()
    telegram_bot_token = sys.argv[9]
    telegram_owner = sys.argv[10]
    thread_body = construct_body()
    sub = "scottishfootball"

    flairs = r.subreddit(sub).flair.link_templates
    flair_texts = ""
    for f in flairs:
        flair_texts += f"{f['text']}, "

    flair_texts = flair_texts[:-2]

    # only select a flair which says match thread (dont worry about capitalisation)
    flair_id = -1
    for flair in flairs:
        if 'discussion'.lower() == flair['text'].lower():
            flair_id = flair['id']

    print(thread_body)

    # thread = r.subreddit(sub).submit("Match Threads, 22/23 Season Rewind",
    #                                               selftext=thread_body,
    #                                               send_replies=False,
    #                                               flair_id=flair_id)
    #
    # thread_link = thread.permalink

    thread_link = "test"

    message = f"'22/'23 rewind thread posted. " + \
              f"(Link to thread: http://www.reddit.com{thread_link})"

    url = f"https://api.telegram.org/bot{telegram_bot_token}/sendMessage?chat_id={telegram_owner}&text={message}"
    requests.post(url)


