# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import praw,urllib,http.cookiejar,re,logging,logging.handlers,datetime,requests,requests.auth,sys,json,unicodedata
import prawcore.exceptions as praw_exception
from praw.models import MoreComments
from time import sleep
import json
import datetime
import mysql.connector
from typing import Dict, List, Tuple, Any, Union
import logging
import os

def setup():
    try:
        admin,username,password,subreddit,user_agent,id,secret,redirect = tuple(sys.argv[1:9])
        log_information(sys.argv)
        r = praw.Reddit(client_id=id, client_secret=secret, username=username, password=password, user_agent=user_agent)
        return r,admin,username,password,subreddit,user_agent,id,secret,redirect
    except:
        sleep(10)







def log_comments(submission, all_com_flairs, top_comment, worst_comment, user_comments):
    submission_comment_flairs = {}
    count = submission.num_comments
    comment_forest = submission.comments
    comment_forest.replace_more(limit=None)

    comments = comment_forest.list()
    query = "INSERT INTO tm.comments (comment_id, body, score, time_posted, commentor, post) VALUES (%s, %s, %s, %s, %s, %s)"
    values = []

    user_list = get_users_in_db()
    user_list = [x for x, *_ in user_list]
    if "deleted" in user_list:
        user_list.append("None")
    comment_list = get_comments_in_db((str(submission)))
    comment_list = [x for x, *_ in comment_list]

    for com in comments:
        if com in comment_list:
            # comment has already been logged. likely caused by new comments being added to the thread since this has last ran
            continue

        usr = com.author
        if str(usr) == "None":
            usr = "deleted"
        else:
            usr = str(usr)

        if usr not in user_list:
            add_user(com)
            user_list.append(usr)

        values.append((str(com),
                       com.body,
                       com.score,
                       datetime.datetime.utcfromtimestamp(com.created_utc),
                       usr,
                       str(com.submission)))
        usr_flair = com.author_flair_text




        if usr in user_comments.keys():
            number, karma = user_comments[usr]
            user_comments[usr] = number + 1, karma+com.score
        else:
            user_comments[usr] = 1, com.score

        # summary of all threads
        if usr_flair in all_com_flairs.keys():
            all_com_flairs[usr_flair] = all_com_flairs[usr_flair] + 1
        else:
            all_com_flairs[usr_flair] = 1

        # submission summary
        if usr_flair in submission_comment_flairs.keys():
            submission_comment_flairs[usr_flair] = submission_comment_flairs[usr_flair] + 1
        else:
            submission_comment_flairs[usr_flair] = 1

        comment_score = com.score

        top_score, _ = top_comment
        worst_score, _ = worst_comment

        if comment_score > top_score:
            top_comment = comment_score, com

        if comment_score < worst_score:
            worst_comment = comment_score, com

    add_many_to_db(query, values)
    return all_com_flairs, submission_comment_flairs, top_comment, worst_comment, user_comments



def get_comments_in_db(post_id):
    query = f"SELECT comment_id FROM tm.comments WHERE post='{post_id}'"
    cursor, mydb = get_cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    return result

def get_users_in_db():
    query = f"SELECT reddit_username FROM tm.users"
    cursor, mydb = get_cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    return result


def add_user(comment):

    proper_flairs = ["No flair", "Rangers", "Celtic", "Hibernian", "Aberdeen",
                     "Heart of Midlothian", "Dundee United", "Ross County", "Livingston",
                     "St Johnstone", "Motherwell", "St Mirren", "Kilmarnock",
                     "Scotland", "Ayr United", "Greenock Morton", "Dundee", "Queen's Park",
                     "Partick Thistle", "Inverness CT", "Raith Rovers", "Elgin City", "Bonnyrigg Rose",
                     "Cove Rangers", "Arbroath", "Hamilton Academical", "Albion Rovers",
                     "Dunfermline Athletic", "Falkirk", "Clyde", "Cowdenbeath", "East Stirlingshire",
                     "Brechin City", "Newton Stewart", "Queen of the South", "Morton Reserves", "Dumbarton",
                     "Kelty Hearts", "Stirling Albion", "Stranraer", "Stenhousemuir", "East Fife", "Linlithgow Rose",
                     "East Kilbride", "Forfar Athletic", "Alloa Athletic"]

    custom_to_actual_flairs = {
        "Rangers": ["A Bag Of Cans And Bassey Back", "Malik Big Baws Tillman", "Alloa's Biggest Bear",
                    "Biggest VL On Here", "Danny Lennon's Island", "definitely won't backfire at all",
                    "Alfredo? He’s singing Sweet Caroline", "Ridvan The Invisible", "Fucklechester Rangers",
                    "Generic Football Fan", "Malik The Cashier Tillman ..", "Biggest VL On Here", "Toight Nups",
                    "Chechnya"],
        "Celtic": ["Red Kola", "Kyogo Scored!", "Royale Union Saint-Gilloise", "Level 7 Parkheidian Bard",
                   "Stupid Sexy O'Riley", "Celtic And Rangers and Royale Union Saint-Gilloise", "Detective Boyle",
                   "Conor Sammon holding a pizza", "Can I just get any flair pls mods can decide",
                   "Callum Davidson Masterclass", "Stale Frankfurt"],
        "Ross County": ["County Til I Die, Fuck Malky Mackay"],
        "Hibernian": ["Real Hibs", "Hibs, Hibs Are Falling Apart Again"],
        "Heart of Midlothian": ["Benny Baningimgimgime"],
        "Aberdeen": ["Japanberdeen"]}

    query = "INSERT INTO tm.users (reddit_username, flair, team) VALUES (%s, %s, %s)"
    usr_flair = comment.author_flair_text
    usr_team = usr_flair
    if usr_flair is None:
        usr_flair = "No flair"
        usr_team = "Unknown"

    if usr_flair not in proper_flairs:
        found = False
        for team, other_flairs in custom_to_actual_flairs.items():
            if usr_flair in other_flairs:
                found = True
                usr_team = team
        if not found:
            log_information(f"{usr_flair} for {comment.author}. https://reddit.com/{comment.permalink}")
            usr_team = "Unknown"

    author = str(comment.author)

    if author == "None":
        author = "deleted"

    values = (author, usr_flair, usr_team)
    query_db(query, values)



def needs_handled(submission):
    query = f"SELECT expected_comments FROM tm.post WHERE post_id='{str(submission)}'"
    cursor, mydb = get_cursor()
    cursor.execute(query)
    result = cursor.fetchone()
    if result is not None:
        log_information(f"{submission.title} is in the database")
        expected_comments = result[0]
        if expected_comments == submission.num_comments:
            query = f"SELECT * FROM tm.comments WHERE post='{str(submission)}'"
            cursor, mydb = get_cursor()
            cursor.execute(query)
            result = cursor.fetchall()
            if len(result) > 0:
                return False
            else:
                log_information(f"{submission.title} has no comments in the comments table, these have probs been missed")
                return True
        else:
            log_information(f"{submission.title} has had more comments added since last time")
            return True
    else:
        log_information(f"{submission.title} is not in the database")
        query = "INSERT INTO tm.post (post_id, title, score, time_posted, team_1, team_2, subreddit, expected_comments) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        team_1, team_2 = re.findall(r"Match Thread: (.*) vs (.*) \| .*", submission.title)[0]
        values = (str(submission),
                  submission.title,
                  submission.score,
                  datetime.datetime.utcfromtimestamp(submission.created_utc),
                  team_1,
                  team_2,
                  str(submission.subreddit),
                  submission.num_comments)
        query_db(query, values)
        return True

def get_cursor():
    mydb = mysql.connector.connect(
        host="mtb-year-review-db",
        user="bot",
        password="bot"
    )
    cursor = mydb.cursor()

    return cursor, mydb

def add_many_to_db(query, values):

    # log_information(f"Executing query {query} with values {values}")
    cursor, mydb = get_cursor()
    cursor.executemany(query, values)
    mydb.commit()
    log_information(f"{cursor.rowcount} record inserted.")


def query_db(query, values):
    log_information(f"Executing query {query} with values {values}")
    cursor, mydb = get_cursor()

    cursor.execute(query, values)
    mydb.commit()
    log_information(f"{cursor.rowcount} record inserted.")


def getTimestamp():
    current_time = datetime.datetime.now()
    day = str(current_time.day) if len(str(current_time.day)) > 1 else '0' + str(current_time.day)
    month = str(current_time.month) if len(str(current_time.month)) > 1 else '0' + str(current_time.month)
    year = str(current_time.year) if len(str(current_time.year)) != 4 else str(current_time.year)[-2:]

    dt = day + '/' + month + '/' + year + ' '
    hr = str(current_time.hour) if len(str(current_time.hour)) > 1 else '0' + str(current_time.hour)
    min = str(current_time.minute) if len(str(current_time.minute)) > 1 else '0' + str(current_time.minute)
    sec = str(current_time.second) if len(str(current_time.second)) > 1 else '0' + str(current_time.second)
    t = '[' + hr + ':' + min + ':' + sec + '] '
    return dt + t


def log_information(message, level=logging.INFO, end="\n"):
    print(f"{getTimestamp()} [{str(message)}]", end=end)
    for match_threader_logger in loggers:
        match_threader_logger.log(level=level, msg=str(message))




# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    LOG_FILENAME = "logs/"
    os.makedirs(os.path.dirname("logs/"), exist_ok=True)
    logging.basicConfig(format='%(asctime)s | %(levelname)s | %(message)s')
    logging_levels = [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL]
    loggers = []
    for logger_level in logging_levels:
        name = logging.getLevelName(logger_level)
        logger = logging.getLogger(name + " logger")
        logger.setLevel(logger_level)
        handler = logging.handlers.RotatingFileHandler(LOG_FILENAME + name + ".LOG",
                                                       maxBytes=100000,
                                                       backupCount=10)
        logger.addHandler(handler)
        loggers.append(logger)

    log_information("STARTUP",
                    level=logging.INFO)

    r, admin, username, password, subreddit, user_agent, id, secret, redirect = setup()
    telegram_bot_token = sys.argv[9]
    telegram_owner = sys.argv[10]
    while(True):
        start_time = datetime.datetime.now()
        log_information(start_time)

        subs = r.redditor("SFMatchThreadder").submissions
        parsed = subs.new(limit=1000000)

        comments = {}
        score = {}
        comment_flairs_total = {}
        user_comments = {}

        total_comments = 0
        avg_time_per_comment = {}

        top_comment = 0, None
        worst_comment = 0, None

        # start_range = datetime.datetime.strptime("1/01/22 00:00:00", '%d/%m/%y %H:%M:%S')
        # end_range = datetime.datetime.strptime("31/12/22 23:59:59", '%d/%m/%y %H:%M:%S')
        count = 0
        for i, submission in enumerate(parsed):
            log_information(f"{submission.title} is the next thread ({submission.permalink})")
            submission_posted = datetime.datetime.utcfromtimestamp(submission.created_utc)
            date = submission_posted.date()
            number_of_comments = submission.num_comments

            if "ScottishFootball" not in str(submission.subreddit):
                continue

            if "Match Thread:" not in submission.title:
                # ignore non match threads
                continue

            # if submission_posted < start_range or submission_posted > end_range:
            #     continue

            if not needs_handled(submission):
                continue

            closest_hundred = str(round(number_of_comments/100)*100)
            if closest_hundred in avg_time_per_comment.keys():
                avg_time, *_ = avg_time_per_comment[closest_hundred]
            else:
                avg_time = datetime.timedelta(seconds=0.1)

            estimated_time = avg_time * number_of_comments

            sub_start = datetime.datetime.now()
            estimated_completion_time = sub_start + estimated_time
            log_information(f"{'-'*50}\nRunning for {datetime.datetime.now()-start_time}.\nAnalysing {submission.title} ({date}). {number_of_comments} comments. Estimated to take {estimated_time} till {estimated_completion_time}")


            game_result = submission.selftext.replace("*", "").split("\n")[0].split(":")[1].strip()
            competition = submission.title.split('|')[1].strip()
            count += 1

            comment_flairs_total, submission_flairs, top_comment, worst_comment, user_comments = log_comments(submission, comment_flairs_total, top_comment, worst_comment, user_comments)

            sub_end = datetime.datetime.now()

            time_taken = sub_end - sub_start
            if estimated_completion_time > sub_end:
                diff = estimated_completion_time - sub_end
            else:
                diff = sub_end - estimated_completion_time
            diff_message = ""
            if diff < datetime.timedelta(seconds=2):
                diff_message = f"Within 2 seconds of expected time. diff={diff}"
            elif diff < datetime.timedelta(seconds=20):
                diff_message = f"Within 20 seconds of expected time. diff={diff}"
            elif diff < datetime.timedelta(minutes=1):
                diff_message = f"Within 1 minute of expected time. diff={diff}"
            elif diff < datetime.timedelta(seconds=2):
                diff_message = f"Within 2 minutes of expected time. diff={diff}"
            else:
                diff_message = f"Outwith 2 minutes of expected time. diff={diff}"

            total_comments += number_of_comments

            if closest_hundred in avg_time_per_comment.keys():
                _, total_count, total_time = avg_time_per_comment[closest_hundred]

            else:
                total_count, total_time = 0, datetime.timedelta(seconds=0)

            total_time += time_taken
            total_count += number_of_comments
            if total_count == 0:
                total_count = 1
            new_avg_time = total_time / total_count
            avg_time_per_comment[closest_hundred] = new_avg_time, total_count, total_time

            log_information(f"Total analysis time is {time_taken} ({diff_message}). Avg time for this submission was {time_taken/number_of_comments if number_of_comments > 0 else time_taken/1}. Overall average time per comment for {closest_hundred} range is now {new_avg_time}")

            for section in sorted(avg_time_per_comment.keys()):
                avg_time, total, time = avg_time_per_comment[section]
                if total > 0:
                    log_information(f"{int(section):03d}: avg of {avg_time} for {total} comments")

            # log_information(f"{submission.title} - {submission.created_utc} - {submission.id} - {submission.subreddit}")

        hour_sleep = 12
        log_information(f"About to go to sleep for {hour_sleep} hours.")
        seconds_sleep = hour_sleep * 60 * 60
        curr_time = datetime.datetime.now()
        end_time = curr_time + datetime.timedelta(seconds=seconds_sleep)
        log_information(f"Current time is {curr_time}. Now sleeping for {seconds_sleep:,} seconds. Predicted to restart at {end_time}.")
        sleep(1)
        sleep(seconds_sleep)



