CREATE TABLE post (
  post_id VARCHAR(7) NOT NULL PRIMARY KEY,  -- reddits post id, will be unique
  title VARCHAR(150) NOT NULL,  -- title of thread
  score INT DEFAULT 0,  -- score of post
  time_posted DATETIME,  -- when was the thread posted
  team_1 VARCHAR(100),  -- name of home team
  team_2 VARCHAR(100),  -- name of away team
  subreddit VARCHAR(50),  -- name of subreddit
  expected_comments INT  -- number of comments reddit thinks it has, not true but helps me know if i need to update it
);