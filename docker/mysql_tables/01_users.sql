CREATE TABLE users (
  reddit_username VARCHAR(25) NOT NULL PRIMARY KEY,  -- reddit username
  flair VARCHAR(100) DEFAULT "No flair",  -- what is their flair
  team VARCHAR(100) DEFAULT "Unknown"  -- what team do they support
);