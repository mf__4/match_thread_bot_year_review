CREATE TABLE comments (
  comment_id VARCHAR(7) NOT NULL PRIMARY KEY,  -- reddits comment id, will be unique
  body VARCHAR(10000),  -- body of comment, useful for analysis
  score INT DEFAULT 0,  -- score of comment
  time_posted DATETIME,  -- when was the comment posted
  commentor VARCHAR(20),  -- reddit username
  post VARCHAR(7),  -- reddits post id, will be unique
  FOREIGN KEY (commentor) REFERENCES users(reddit_username) ON DELETE SET NULL,
  FOREIGN KEY (post) REFERENCES post(post_id) ON DELETE SET NULL
);